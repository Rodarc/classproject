// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APlayerCharacter::APlayerCharacter() {
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);
	
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	bCanFire = true;
	ShootingRange = 3000.0f;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Axis
	InputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
	InputComponent->BindAxis("Turn", this, &ACharacter::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &ACharacter::AddControllerPitchInput);

	// Actions
	InputComponent->BindAction("Fire", IE_Pressed, this, &APlayerCharacter::FirePressed);
}

float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	HealthComponent->AddHealth(-DamageAmount);
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void APlayerCharacter::MoveForward(float AxisValue) {
	AddMovementInput(UKismetMathLibrary::GetForwardVector(FRotator(0.0f, GetControlRotation().Yaw, 0.0f)), AxisValue);
}

void APlayerCharacter::MoveRight(float AxisValue) {
	AddMovementInput(UKismetMathLibrary::GetRightVector(FRotator(0.0f, GetControlRotation().Yaw, 0.0f)), AxisValue);
}

void APlayerCharacter::FirePressed() {
	if (bCanFire) {
		bCanFire = false;
		PlayAnimMontage(FireMontage);
		GetWorldTimerManager().SetTimer(FireCooldownTimer, this, &APlayerCharacter::ResetFire, 0.2, false);
	}
}

void APlayerCharacter::ResetFire() {
	bCanFire = true;
}

void APlayerCharacter::Shoot_Implementation() {
	UGameplayStatics::SpawnEmitterAttached(MuzzleParticles, GetMesh(), "SMG_Barrel");

	TArray<TEnumAsByte<EObjectTypeQuery> > TiposObjetos;
	TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery1);
	TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery3);

	TArray<AActor *> IgnoredActors;
	IgnoredActors.Add(this);

	FHitResult HitResult;
	bool trace = UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), Camera->GetComponentLocation(), Camera->GetComponentLocation() + Camera->GetForwardVector() * ShootingRange,
		TiposObjetos, false, IgnoredActors, EDrawDebugTrace::None, HitResult, true);

	if (trace) {
		// instanciar particulas
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticles, HitResult.Location, UKismetMathLibrary::MakeRotFromX(HitResult.Normal));
		HitResult.GetActor()->TakeDamage(10.0f, FDamageEvent(), GetController(), this);
	}
}

