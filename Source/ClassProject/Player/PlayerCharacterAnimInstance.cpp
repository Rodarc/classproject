// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacterAnimInstance.h"
#include "PlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UPlayerCharacterAnimInstance::NativeInitializeAnimation() {
	Super::NativeInitializeAnimation();

	APlayerCharacter * PlayerPawn = Cast<APlayerCharacter>(TryGetPawnOwner());
	if (PlayerPawn) {
		PlayerPawn->OnBeginAim.AddUniqueDynamic(this, &UPlayerCharacterAnimInstance::OnBeginAim);
		PlayerPawn->OnEndAim.AddUniqueDynamic(this, &UPlayerCharacterAnimInstance::OnEndAim);
	}
}

void UPlayerCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds) {
	Super::NativeUpdateAnimation(DeltaSeconds);

	APawn * PawnOwner = TryGetPawnOwner();
	APlayerCharacter * PlayerCharacter = Cast<APlayerCharacter>(PawnOwner);

	if (PlayerCharacter) {
		bAccelerating = PlayerCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.0f;
	}

	if(PawnOwner) {// PawnOwner != nullptr

		// Actualizar velocidad
		Speed = PawnOwner->GetMovementComponent()->Velocity.Size();


		// Actualizar angulo de direccion
		FRotator VelocityRotator = PawnOwner->GetMovementComponent()->Velocity.Rotation();

		FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(PawnOwner->GetControlRotation(), VelocityRotator);
		DirectionAngle = Delta.Yaw * -1;

		// Actualizar el root offset
		if (bUpdateRootOffset) {
			FRotator DeltaControlRotation = ControlRotationLastFrame - PawnOwner->GetControlRotation();
			DeltaControlRotation.Normalize();
			RootYawOffset += DeltaControlRotation.Yaw;

			if (GetCurveValue(FName("Turning")) == 1.0f) {
				TurningValue = GetCurveValue(FName("DistanceCurve")) + 90.0f;
				float DeltaTurningValue = TurningValue - TurningValueLastFrame;
				if (GetCurveValue(FName("TurningRight")) == 1.0f) {
					DeltaTurningValue *= -1;
				}
				RootYawOffset -= DeltaTurningValue;
			}
			else {
				TurningValue = 0.0f;
			}
			
		}
		else {
			// RootYawOffset = 0.0f;
			if (FMath::Abs(RootYawOffset) > 0.0f) {
				RootYawOffset = UKismetMathLibrary::FInterpTo_Constant(RootYawOffset, 0.0f, DeltaSeconds, 540.0f);
			}
		}

		// Actualizar datos del ultimo frame
		ControlRotationLastFrame = PawnOwner->GetControlRotation();
		TurningValueLastFrame = TurningValue;
	}
}

void UPlayerCharacterAnimInstance::OnBeginAim() {
	bAiming = true;
}
void UPlayerCharacterAnimInstance::OnEndAim() {
	bAiming = false;
}
