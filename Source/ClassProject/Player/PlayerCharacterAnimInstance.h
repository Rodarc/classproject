// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PlayerCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class CLASSPROJECT_API UPlayerCharacterAnimInstance : public UAnimInstance {
	GENERATED_BODY()

	public:

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bAiming;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bAccelerating;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bUpdateRootOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Speed;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DirectionAngle;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StopDirectionAngle;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RootYawOffset;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TurningValue;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TurningValueLastFrame;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator ControlRotationLastFrame;

	UFUNCTION()
	void OnBeginAim();
	
	UFUNCTION()
	void OnEndAim();
	
};
