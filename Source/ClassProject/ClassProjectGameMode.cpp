// Copyright Epic Games, Inc. All Rights Reserved.

#include "ClassProjectGameMode.h"
#include "ClassProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AClassProjectGameMode::AClassProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
