// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ClassProject/Gameplay/HealthComponent.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEnemyDied, AActor *, Enemy);

UCLASS()
class CLASSPROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UHealthComponent * HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	bool bCanFire;

	void FirePressed();

	void ResetFire();

	FTimerHandle FireCooldownTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UAnimMontage * FireMontage;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Shoot();
	void Shoot_Implementation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UParticleSystem * MuzzleParticles;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UParticleSystem * HitParticles;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FEnemyDied EnemyDied;
};
